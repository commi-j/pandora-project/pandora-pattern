# Pandora Pattern

[![RELEASE](https://img.shields.io/maven-central/v/tk.labyrinth.pandora/pandora-pattern.svg?label=Release&style=flat-square)](https://mvnrepository.com/artifact/tk.labyrinth.pandora/pandora-pattern)
[![PIPELINE_MASTER](https://gitlab.com/commi-j/pandora-project/pandora-pattern/badges/master/pipeline.svg?key_text=Master&key_width=50&style=flat-square)](https://gitlab.com/commi-j/pandora-project/pandora-pattern/-/pipelines)
[![TEST_COVERAGE_MASTER](https://gitlab.com/commi-j/pandora-project/pandora-pattern/badges/master/coverage.svg?key_text=Coverage&style=flat-square)](https://gitlab.com/commi-j/pandora-project/pandora-pattern/-/jobs)

[![SNAPSHOT](https://img.shields.io/nexus/s/tk.labyrinth.pandora/pandora-pattern?label=Snapshot&server=https%3A%2F%2Foss.sonatype.org&style=flat-square)](https://oss.sonatype.org/#nexus-search;gav~tk.labyrinth.pandora~pandora-pattern~~~)
[![PIPELINE_DEV](https://gitlab.com/commi-j/pandora-project/pandora-pattern/badges/dev/pipeline.svg?key_text=Dev&key_width=35&style=flat-square)](https://gitlab.com/commi-j/pandora-project/pandora-pattern/-/pipelines)
[![TEST_COVERAGE_DEV](https://gitlab.com/commi-j/pandora-project/pandora-pattern/badges/dev/coverage.svg?key_text=Coverage&style=flat-square)](https://gitlab.com/commi-j/pandora-project/pandora-pattern/-/jobs)

[![LICENSE](https://img.shields.io/gitlab/license/commi-j/pandora-project/pandora-pattern?color=q&label=License&style=flat-square)](LICENSE)

## Some Docs

- [Pandora Pattern @ Notion](https://commitman.notion.site/Pandora-Pattern-bc23b4015e9a46c29d668f3c1f9c7aa1)