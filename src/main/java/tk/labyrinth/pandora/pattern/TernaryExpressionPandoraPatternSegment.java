package tk.labyrinth.pandora.pattern;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.pattern.misc4j.PandoraPatternStringUtils;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class TernaryExpressionPandoraPatternSegment implements PandoraPatternSegment {

	@NonNull
	String falseExpression;

	// TODO: Replace with conditionExpression.
	@NonNull
	String name;

	@NonNull
	String trueExpression;

	@Nullable
	@Override
	public String findPlaceholderFalseExpression(String placeholderName) {
		return Objects.equals(placeholderName, name) ? falseExpression : null;
	}

	@Override
	public String getJavaPatternSegment() {
		String result;
		{
			String optionalPart = falseExpression.isEmpty() ? "?" : "";
			//
			if (Objects.equals(trueExpression, "$")) {
				result = "(?<%s>.+?)%s".formatted(name, optionalPart);
			} else {
				// TODO: Check case where there is an escaped dollar in the expression.
				//
				result = "(%s)%s".formatted(
						trueExpression.replace("$", "(?<%s>.+?)".formatted(name)),
						optionalPart);
			}
		}
		return result;
	}

	@Override
	public List<String> getPlaceholderNames() {
		return List.of(name);
	}

	@Override
	public String render(RenderPlaceholderFunction renderPlaceholderFunction) {
		String result;
		{
			String value = renderPlaceholderFunction.render(name);
			//
			if (value != null) {
				result = trueExpression.replace("$", value);
			} else {
				result = falseExpression;
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return "${%s?%s:%s}".formatted(name, escape(trueExpression), escape(falseExpression));
	}

	public static String escape(String string) {
		return string
				.replace("\\", "\\\\")
				.replace("?", "\\?")
				.replace(":", "\\:")
				.replace("}", "\\}");
	}

	@Nullable
	public static Integer findIndexOfUnescapedCharacter(String string, char character, int from) {
		Integer result;
		{
			int firstIndex = string.indexOf(character, from);
			//
			if (firstIndex != -1) {
				if (!PandoraPatternStringUtils.isCharacterEscaped(string, firstIndex)) {
					result = firstIndex;
				} else {
					result = findIndexOfUnescapedCharacter(string, character, firstIndex + 1);
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	public static TernaryExpressionPandoraPatternSegment from(String expression) {
		TernaryExpressionPandoraPatternSegment result;
		{
			boolean isExpandedForm = expression.charAt(1) == '{';
			//
			if (isExpandedForm) {
				Integer indexOfQuestionMark = findIndexOfUnescapedCharacter(expression, '?', 2);
				//
				if (indexOfQuestionMark != null) {
					// Expression form.
					//
					Integer indexOfColon = findIndexOfUnescapedCharacter(expression, ':', indexOfQuestionMark + 1);
					//
					result = TernaryExpressionPandoraPatternSegment.builder()
							.falseExpression(indexOfColon != null
									? unescape(expression.substring(indexOfColon + 1, expression.length() - 1))
									: "null")
							.name(expression.substring(2, indexOfQuestionMark))
							.trueExpression(indexOfColon != null
									? unescape(expression.substring(indexOfQuestionMark + 1, indexOfColon))
									: unescape(expression.substring(indexOfQuestionMark + 1, expression.length() - 1)))
							.build();
				} else {
					// Canonical form.
					//
					Integer indexOfColon = findIndexOfUnescapedCharacter(expression, ':', 0);
					//
					result = TernaryExpressionPandoraPatternSegment.builder()
							.falseExpression(indexOfColon != null
									? unescape(expression.substring(indexOfColon + 1, expression.length() - 1))
									: "null")
							.name(indexOfColon != null
									? expression.substring(2, indexOfColon)
									: expression.substring(2, expression.length() - 1))
							.trueExpression("$")
							.build();
				}
			} else {
				// Simple form.
				//
				String name = expression.substring(1);
				//
				result = TernaryExpressionPandoraPatternSegment.builder()
						.falseExpression("null")
						.name(name)
						.trueExpression("$")
						.build();
			}
		}
		return result;
	}

	public static String unescape(String string) {
		return string
				.replace("\\\\", "\\")
				.replace("\\?", "?")
				.replace("\\:", ":")
				.replace("\\}", "}");
	}
}
