package tk.labyrinth.pandora.pattern;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;

public interface PandoraPatternSegment {

	@Nullable
	String findPlaceholderFalseExpression(String placeholderName);

	String getJavaPatternSegment();

	// TODO: Are we sure there could be more than 1?
	List<String> getPlaceholderNames();

	/**
	 * @param renderPlaceholderFunction placeholderName -> value
	 *
	 * @return non-null
	 */
	String render(RenderPlaceholderFunction renderPlaceholderFunction);

	@Override
	String toString();
}
