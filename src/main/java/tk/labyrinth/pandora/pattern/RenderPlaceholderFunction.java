package tk.labyrinth.pandora.pattern;

import org.checkerframework.checker.nullness.qual.Nullable;

public interface RenderPlaceholderFunction {

	@Nullable
	String render(String placeholderName);
}
