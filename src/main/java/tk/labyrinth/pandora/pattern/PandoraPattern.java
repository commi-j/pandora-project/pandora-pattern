package tk.labyrinth.pandora.pattern;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.pattern.misc4j.PandoraPatternStringUtils;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * - Simple form:<br>
 * $foo<br>
 * Ends when first non-alphabetic nor numeric character is encountered.<br>
 * <br>
 * - Expanded form:<br>
 * ${foo}<br>
 * ${foo.bar}<br>
 * Ends with close bracket and allows chained name.<br>
 * <br>
 * - Expression form:<br>
 * ${foo?$:null} - same as $foo, will render the value of foo or 'null' if foo is null.<br>
 * ${foo:} - will render empty string if foo is null.<br>
 * ${foo?#$#} - will render the part after '?' replacing '$' with the variable value if it's not null, 'null'
 * otherwise.<br>
 *
 * @author Commitman
 * @version 1.2.0
 */
// TODO: Decide proper rule set for null values, e.g. put 'null', empty string or leave placeholder.
@Value(staticConstructor = "of")
public class PandoraPattern {

	@NonNull
	List<PandoraPatternSegment> segments;

	public Pattern getJavaPattern() {
		return Pattern.compile(segments.map(PandoraPatternSegment::getJavaPatternSegment).mkString());
	}

	public String getPlaceholderFalseExpression(String placeholderName) {
		return segments
				.map(segment -> segment.findPlaceholderFalseExpression(placeholderName))
				.filter(Objects::nonNull)
				.get();
	}

	public List<String> getPlaceholderNames() {
		return segments.flatMap(PandoraPatternSegment::getPlaceholderNames).distinct();
	}

	public Map<String, String> parse(String value) {
		Map<String, String> result;
		{
			Pattern regexPattern = Pattern.compile("^%s$".formatted(getJavaPattern()));
			//
			Matcher matcher = regexPattern.matcher(value);
			//
			if (matcher.matches()) {
				result = getPlaceholderNames().toMap(
						Function.identity(),
						placeholderName -> {
							String matchedValue = matcher.group(placeholderName);
							//
							return !Objects.equals(matchedValue, getPlaceholderFalseExpression(placeholderName))
									? matchedValue
									: null;
						});
			} else {
				throw new IllegalArgumentException("Pattern does not match: pattern = '%s', value = '%s'".formatted(
						toString(), value));
			}
		}
		return result;
	}

	/**
	 * @param variables non-null
	 *
	 * @return non-null
	 */
	public String render(Map<String, String> variables) {
		return render(variableName -> variables.get(variableName).getOrNull());
	}

	/**
	 * @param renderPlaceholderFunction placeholderName -> value
	 *
	 * @return non-null
	 */
	public String render(RenderPlaceholderFunction renderPlaceholderFunction) {
		return segments.map(segment -> segment.render(renderPlaceholderFunction)).mkString();
	}

	@JsonValue
	@Override
	public String toString() {
		return segments.mkString();
	}

	private static int indexOfFirstMatching(String value, IntPredicate characterPredicate, int fromIndex) {
		return IntStream.range(fromIndex, value.length())
				.filter(index -> characterPredicate.test(value.charAt(index)))
				.findFirst()
				.orElse(-1);
	}

	private static List<PandoraPatternSegment> splitBySegments(String head, String tail) {
		List<PandoraPatternSegment> result;
		{
			int indexOfDollar = tail.indexOf('$');
			//
			if (indexOfDollar != -1) {
				if (!PandoraPatternStringUtils.isCharacterEscaped(tail, indexOfDollar)) {
					String plainText = "%s%s".formatted(head, tail.substring(0, indexOfDollar));
					//
					boolean isCanonicalForm = tail.charAt(indexOfDollar + 1) == '{';
					//
					if (isCanonicalForm) {
						int indexOfCloseBrace = tail.indexOf('}', indexOfDollar + 2);
						//
						result = List
								.of(
										!plainText.isEmpty() ? PlainTextPandoraPatternSegment.of(plainText) : null,
										TernaryExpressionPandoraPatternSegment.from(tail.substring(
												indexOfDollar,
												indexOfCloseBrace + 1)))
								.filter(Objects::nonNull)
								.appendAll(splitBySegments("", tail.substring(indexOfCloseBrace + 1)));
					} else {
						int rawIndexOfNameEnd = indexOfFirstMatching(
								tail,
								character -> !Character.isAlphabetic(character) &&
										!Character.isDigit(character),
								indexOfDollar + 1);
						//
						int indexOfNameEnd = rawIndexOfNameEnd != -1 ? rawIndexOfNameEnd : tail.length();
						//
						result = List
								.of(
										!plainText.isEmpty() ? PlainTextPandoraPatternSegment.of(plainText) : null,
										TernaryExpressionPandoraPatternSegment.from(tail.substring(
												indexOfDollar,
												indexOfNameEnd)))
								.filter(Objects::nonNull)
								.appendAll(splitBySegments("", tail.substring(indexOfNameEnd)));
					}
				} else {
					result = splitBySegments(
							"%s%s".formatted(head, tail.substring(indexOfDollar + 1)),
							tail.substring(indexOfDollar + 1));
				}
			} else {
				String plainText = "%s%s".formatted(head, tail);
				//
				result = !plainText.isEmpty() ? List.of(PlainTextPandoraPatternSegment.of(plainText)) : List.empty();
			}
		}
		return result;
	}

	@JsonCreator
	public static PandoraPattern from(String patternString) {
		return of(splitBySegments("", patternString));
	}

	public static Map<String, String> parse(String patternString, String value) {
		return from(patternString).parse(value);
	}

	/**
	 * @param patternString non-null
	 * @param variables     non-null
	 *
	 * @return non-null
	 */
	public static String render(String patternString, Map<String, String> variables) {
		return from(patternString).render(variables);
	}

	/**
	 * @param renderPlaceholderFunction placeholderName -> value
	 *
	 * @return non-null
	 */
	public static String render(String patternString, RenderPlaceholderFunction renderPlaceholderFunction) {
		return from(patternString).render(renderPlaceholderFunction);
	}
}
