package tk.labyrinth.pandora.pattern;

import io.vavr.collection.List;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

@Value(staticConstructor = "of")
public class PlainTextPandoraPatternSegment implements PandoraPatternSegment {

	String text;

	@Nullable
	@Override
	public String findPlaceholderFalseExpression(String placeholderName) {
		return null;
	}

	@Override
	public String getJavaPatternSegment() {
		// TODO: Properly escape for regex.
		return text;
	}

	@Override
	public List<String> getPlaceholderNames() {
		return List.empty();
	}

	@Override
	public String render(RenderPlaceholderFunction renderPlaceholderFunction) {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}
}
