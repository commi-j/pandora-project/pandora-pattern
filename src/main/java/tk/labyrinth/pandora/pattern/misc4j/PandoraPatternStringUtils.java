package tk.labyrinth.pandora.pattern.misc4j;

/**
 * Lightweight StringUtils version designed for Pandora Pattern library.
 *
 * @author Commitman
 * @version 1.0.5
 */
public class PandoraPatternStringUtils {

	/**
	 * @param value          non-empty
	 * @param characterIndex greater than or equal to 0, less than value.length()
	 *
	 * @return true if escaped, false otherwise
	 *
	 * @since 1.0.5
	 */
	public static boolean isCharacterEscaped(String value, int characterIndex) {
		boolean result;
		{
			if (characterIndex > 0) {
				char previousCharacter = value.charAt(characterIndex - 1);
				//
				if (previousCharacter == '\\') {
					return !isCharacterEscaped(value, characterIndex - 1);
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
		return result;
	}
}
