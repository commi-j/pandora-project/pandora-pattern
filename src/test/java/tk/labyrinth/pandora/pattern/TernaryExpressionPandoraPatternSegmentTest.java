package tk.labyrinth.pandora.pattern;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class TernaryExpressionPandoraPatternSegmentTest {

	@Test
	void testFrom() {
		Assertions
				.assertThat(TernaryExpressionPandoraPatternSegment.from("$name"))
				.isEqualTo(TernaryExpressionPandoraPatternSegment.builder()
						.falseExpression("null")
						.name("name")
						.trueExpression("$")
						.build());
		Assertions
				.assertThat(TernaryExpressionPandoraPatternSegment.from("${value?\\:$:}"))
				.isEqualTo(TernaryExpressionPandoraPatternSegment.builder()
						.falseExpression("")
						.name("value")
						.trueExpression(":$")
						.build());
	}
}