package tk.labyrinth.pandora.pattern;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PandoraPatternTest {

	@Test
	void testFromWithCanonicalFormWithDotPattern() {
		Assertions
				.assertThat(PandoraPattern.from("${one.two}"))
				.isEqualTo(PandoraPattern.of(List.of(
						TernaryExpressionPandoraPatternSegment.builder()
								.falseExpression("null")
								.name("one.two")
								.trueExpression("$")
								.build())));
	}

	@Test
	void testFromWithExpressionFormPattern() {
		Assertions
				.assertThat(PandoraPattern.from("$name${value? \\: $}"))
				.isEqualTo(PandoraPattern.of(List.of(
						TernaryExpressionPandoraPatternSegment.builder()
								.falseExpression("null")
								.name("name")
								.trueExpression("$")
								.build(),
						TernaryExpressionPandoraPatternSegment.builder()
								.falseExpression("null")
								.name("value")
								.trueExpression(" : $")
								.build())));
	}

	@Test
	void testFromWithExpressionFormPatternAndEmptyDefaultValue() {
		Assertions
				.assertThat(PandoraPattern.from("$name:${value?$:}"))
				.isEqualTo(PandoraPattern.of(List.of(
						TernaryExpressionPandoraPatternSegment.builder()
								.falseExpression("null")
								.name("name")
								.trueExpression("$")
								.build(),
						PlainTextPandoraPatternSegment.of(":"),
						TernaryExpressionPandoraPatternSegment.builder()
								.falseExpression("")
								.name("value")
								.trueExpression("$")
								.build())));
	}

	@Test
	void testFromWithSimpleFormPattern() {
		Assertions
				.assertThat(PandoraPattern.from("$name : $value"))
				.isEqualTo(PandoraPattern.of(List.of(
						TernaryExpressionPandoraPatternSegment.builder()
								.falseExpression("null")
								.name("name")
								.trueExpression("$")
								.build(),
						PlainTextPandoraPatternSegment.of(" : "),
						TernaryExpressionPandoraPatternSegment.builder()
								.falseExpression("null")
								.name("value")
								.trueExpression("$")
								.build())));
	}

	@Test
	void testGetJavaPatternWithExpressionForm() {
		Assertions
				.assertThat(PandoraPattern.from("$foo${bar?\\:$}").getJavaPattern().pattern())
				.isEqualTo("(?<foo>.+?)(:(?<bar>.+?))");
	}

	@Test
	void testGetJavaPatternWithFalseExpressions() {
		{
			// Default: falseExpression = "null"
			//
			PandoraPattern pandoraPattern = PandoraPattern.from("foo/bar/$val");
			//
			Assertions
					.assertThat(pandoraPattern.getJavaPattern().pattern())
					.isEqualTo("foo/bar/(?<val>.+?)");
			//
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/VAL"))
					.isEqualTo(HashMap.of("val", "VAL"));
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/null"))
					.isEqualTo(HashMap.of("val", null));
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/nil"))
					.isEqualTo(HashMap.of("val", "nil"));
			Assertions
					.assertThatThrownBy(() -> pandoraPattern.parse("foo/bar/"))
					.isInstanceOf(IllegalArgumentException.class)
					.hasMessage("Pattern does not match: pattern = 'foo/bar/${val?$:null}', value = 'foo/bar/'");
		}
		{
			// Empty: falseExpression = ""
			//
			PandoraPattern pandoraPattern = PandoraPattern.from("foo/bar/${val:}");
			//
			Assertions
					.assertThat(pandoraPattern.getJavaPattern().pattern())
					.isEqualTo("foo/bar/(?<val>.+?)?");
			//
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/VAL"))
					.isEqualTo(HashMap.of("val", "VAL"));
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/null"))
					.isEqualTo(HashMap.of("val", "null"));
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/nil"))
					.isEqualTo(HashMap.of("val", "nil"));
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/"))
					.isEqualTo(HashMap.of("val", null));
		}
		{
			// Custom: falseExpression = "nil"
			//
			PandoraPattern pandoraPattern = PandoraPattern.from("foo/bar/${val:nil}");
			//
			Assertions
					.assertThat(pandoraPattern.getJavaPattern().pattern())
					.isEqualTo("foo/bar/(?<val>.+?)");
			//
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/VAL"))
					.isEqualTo(HashMap.of("val", "VAL"));
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/null"))
					.isEqualTo(HashMap.of("val", "null"));
			Assertions
					.assertThat(pandoraPattern.parse("foo/bar/nil"))
					.isEqualTo(HashMap.of("val", null));
			Assertions
					.assertThatThrownBy(() -> pandoraPattern.parse("foo/bar/"))
					.isInstanceOf(IllegalArgumentException.class)
					.hasMessage("Pattern does not match: pattern = 'foo/bar/${val?$:nil}', value = 'foo/bar/'");
		}
	}

	@Test
	void testGetJavaPatternWithSimpleForm() {
		Assertions
				.assertThat(PandoraPattern.from("$foo").getJavaPattern().pattern())
				.isEqualTo("(?<foo>.+?)");
	}

	@Test
	void testGetJavaPatternWithThreePlaceholders() {
		Assertions
				.assertThat(PandoraPattern.from("$first:$second:$third").getJavaPattern().pattern())
				.isEqualTo("(?<first>.+?):(?<second>.+?):(?<third>.+?)");
	}

	@Test
	void testParseWithBothValuesPresent() {
		Assertions
				.assertThat(PandoraPattern.parse("$foo${bar?\\:$}", "FOO:BAR"))
				.isEqualTo(HashMap.of(
						"foo", "FOO",
						"bar", "BAR"));
	}

	@Test
	void testParseWithElseSectionPresent() {
		Assertions
				.assertThat(PandoraPattern.parse("${foo:nil}", "FOO"))
				.isEqualTo(HashMap.of("foo", "FOO"));
		Assertions
				.assertThat(PandoraPattern.parse("${foo:nil}", "nil"))
				.isEqualTo(HashMap.of("foo", null));
	}

	@Test
	void testParseWithMismatch() {
		Assertions
				.assertThatCode(() -> PandoraPattern.parse("foo/$bar", "bar/foo"))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Pattern does not match: pattern = 'foo/${bar?$:null}', value = 'bar/foo'");
	}

	@Test
	void testParseWithOneValue() {
		Assertions
				.assertThat(PandoraPattern.parse("$foo${bar?\\:$:}", "FOO"))
				.isEqualTo(HashMap.of(
						"foo", "FOO",
						"bar", null));
	}

	@Test
	void testParseWithThenSection() {
		PandoraPattern pandoraPattern = PandoraPattern.from("$name${value?\\:$:}");
		Assertions
				.assertThat(pandoraPattern.parse("a"))
				.isEqualTo(HashMap.of(
						"name", "a",
						"value", null));
		Assertions
				.assertThat(pandoraPattern.parse("a:b"))
				.isEqualTo(HashMap.of(
						"name", "a",
						"value", "b"));
	}

	@Test
	void testRenderWithCanonicalForm() {
		Assertions
				.assertThat(PandoraPattern.render("${foo}", HashMap.of("foo", "FOO_BAR")))
				.isEqualTo("FOO_BAR");
	}

	@Test
	void testRenderWithCanonicalFormAndDefaultEmptyValueAndNoValueProvided() {
		Assertions
				.assertThat(PandoraPattern.render("${foo.bar?$:}", HashMap.of("foo.bar", null)))
				.isEqualTo("");
	}

	@Test
	void testRenderWithCanonicalFormAndDefaultValueAndNoValueProvided() {
		Assertions
				.assertThat(PandoraPattern.render("${foo.bar?$:default}", HashMap.of("foo.bar", null)))
				.isEqualTo("default");
	}

	@Test
	void testRenderWithCanonicalFormAndNoValue() {
		Assertions
				.assertThat(PandoraPattern.render("${foo.bar}", HashMap.of("foo.bar", null)))
				.isEqualTo("null");
	}

	@Test
	void testRenderWithCanonicalFormWithDot() {
		Assertions
				.assertThat(PandoraPattern.render("${foo.bar}", HashMap.of("foo.bar", "FOO_BAR")))
				.isEqualTo("FOO_BAR");
	}

	@Test
	void testRenderWithExpressionFormAndBothValuesPresent() {
		Assertions
				.assertThat(PandoraPattern.render(
						"$foo:${bar?$}",
						HashMap.of(
								"foo", "FOO",
								"bar", "BAR")))
				.isEqualTo("FOO:BAR");
	}

	@Test
	void testRenderWithExpressionFormAndDefaultValueAndNoMatchingValue() {
		Assertions
				.assertThat(PandoraPattern.render("$foo:${bar?$:nil}", HashMap.of("foo", "FOO")))
				.isEqualTo("FOO:nil");
	}

	@Test
	void testRenderWithExpressionFormAndNoMatchingValue() {
		Assertions
				.assertThat(PandoraPattern.render("$foo:${bar?$:}", HashMap.of("foo", "FOO")))
				.isEqualTo("FOO:");
	}

	@Test
	void testRenderWithExpressionFormWithFalseSectionWhenValueIsAbsent() {
		Assertions
				.assertThat(PandoraPattern.render("${foo:nil}", HashMap.empty()))
				.isEqualTo("nil");
	}

	@Test
	void testRenderWithExpressionFormWithFalseSectionWhenValueIsPresent() {
		Assertions
				.assertThat(PandoraPattern.render("${foo:nil}", HashMap.of("foo", "FOO")))
				.isEqualTo("FOO");
	}

	@Test
	void testRenderWithExpressionFormWithoutThenAndNoMatchingValue() {
		Assertions
				.assertThat(PandoraPattern.render("$foo:${bar?$}", HashMap.of("foo", "FOO")))
				.isEqualTo("FOO:null");
	}

	@Test
	void testRenderWithSimpleAndCanonicalForm() {
		Assertions
				.assertThat(PandoraPattern.render(
						"pre$simple${canonical}post",
						HashMap.of(
								"simple", "Simple",
								"canonical", "CANONICAL")))
				.isEqualTo("preSimpleCANONICALpost");
	}

	@Test
	void testRenderWithSimpleForm() {
		Assertions
				.assertThat(PandoraPattern.render("$foo", HashMap.of("foo", "FOO")))
				.isEqualTo("FOO");
	}

	@Test
	void testRenderWithSimpleFormAndEmptyMap() {
		Assertions
				.assertThat(PandoraPattern.render("$foo", HashMap.empty()))
				.isEqualTo("null");
	}

	@Test
	void testRenderWithTagExample() {
		PandoraPattern pattern = PandoraPattern.from("$name${value?\\:$:}");
		//
		Assertions
				.assertThat(pattern.render(HashMap.of("name", "foo")))
				.isEqualTo("foo");
		Assertions
				.assertThat(pattern.render(HashMap.of(
						"name", "foo",
						"value", null)))
				.isEqualTo("foo");
		Assertions
				.assertThat(pattern.render(HashMap.of(
						"name", "foo",
						"value", "BAR")))
				.isEqualTo("foo:BAR");
	}

	/**
	 * Special case to ensure it works when value contains regex special characters.
	 */
	@Test
	void testRenderWithValueSimilarToRegexGroup() {
		Assertions
				.assertThat(PandoraPattern.render(
						"$foo:$bar",
						HashMap.of(
								"foo", "hello",
								"bar", "$world")))
				.isEqualTo("hello:$world");
	}

	@Test
	void testToString() {
		Assertions
				.assertThat(PandoraPattern.from("$name${value? \\: $}").toString())
				.isEqualTo("${name?$:null}${value? \\: $:null}");
	}
}