@echo off
setlocal

set "JACOCO_CSV=target\site\jacoco\jacoco.csv"

set "instructions=0"
set "covered=0"

for /f "usebackq tokens=4,5 delims=," %%a in ("%JACOCO_CSV%") do (
    set /a "instructions+=%%a+%%b"
    set /a "covered+=%%b"
)

echo %covered% / %instructions% instructions covered
set /a "coverage=100*covered/instructions"
echo %coverage%%% covered

endlocal
