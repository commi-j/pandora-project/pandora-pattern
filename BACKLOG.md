
- Make an object for placeholder, make it available when analyzing pattern or when computing variable names;
- Make it possible to return the placeholder itself in case of no value;
  - There should be possibility to do in on pattern and on variableResolver levels;
